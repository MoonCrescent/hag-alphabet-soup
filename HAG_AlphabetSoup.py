#!/bin/python3

def word_search(grid, Nr, Nc, word):
    # Assumption: 
    # 1) A word would appear only once in a specific row, col or diag
    # 2) Min word length = 2, so two single-char at top-right & bottom-left are not searchable
    # 3) Word length matters to commence search
    length = len(word)
    # Reverse via extended slice
    rev_word = word[::-1]
    # print(rev_word, "\n")

    Nd = min(Nc,Nr) # Main diagonal length

    if length <= Nc:
        # Search by row
        for ind in range(Nr):
            row = "".join(grid[ind][:])
            if word in row:
                start = row.index(word)
                print(word, str(ind) + ":" + str(start), str(ind) + ":" + str(start+length-1))
            elif rev_word in row:
                end = row.index(rev_word)
                print(word, str(ind) + ":" + str(end+length-1), str(ind) + ":" + str(end))
    
    if length <= Nr:
        # Search by column
        for ind in range(Nc):
            col = "".join(grid[ind2][ind] for ind2 in range(Nr))
            if word in col:
                # print("column: ", col)
                start = col.index(word)
                print(word, str(start) + ":" + str(ind), str(start+length-1) + ":" + str(ind))
            elif rev_word in col:
                # print("column: ", col)
                end = col.index(rev_word)
                print(word, str(end+length-1) + ":" + str(ind), str(end) + ":" + str(ind))
        
    if length <= Nd:
        # Search main diagonal, then possibly upper and lower off-diagonal
        main_diag = "".join(grid[ind][ind] for ind in range(Nd))
        if word in main_diag:
            start = main_diag.index(word)
            print(word, str(start) + ":" + str(start), str(start+length-1) + ":" + str(start+length-1))
        elif rev_word in main_diag:
            end = main_diag.index(rev_word)
            print(word, str(end+length-1) + ":" + str(end+length-1), str(end) + ":" + str(end))
        # Search upper right off-diagonals
        for offset in range(1, Nc-1):
            off_diag = "".join(grid[ind][ind+offset] for ind in range(Nr) if ind+offset < Nc)
            # Check off_diag length
            if length <= len(off_diag): 
                if word in off_diag:
                    start = off_diag.index(word)
                    print(word, str(start) + ":" + str(start+offset), str(start+length-1) + ":" + str(start+offset+length-1))
                elif rev_word in off_diag:
                    end = off_diag.index(rev_word)
                    print(word, str(end+length-1) + ":" + str(end+offset+length-1), str(end) + ":" + str(end+offset))
        # Search lower left off-diagonals
        for offset in range(1, Nr-1):
            off_diag = "".join(grid[ind+offset][ind] for ind in range(Nc) if ind+offset < Nr)
            # Check off_diag length
            if length <= len(off_diag): 
                if word in off_diag:
                    start = off_diag.index(word)
                    print(word, str(start+offset) + ":" + str(start), str(start+offset+length-1) + ":" + str(start+length-1))
                elif rev_word in off_diag:
                    end = off_diag.index(rev_word)
                    print(word, str(end+offset+length-1) + ":" + str(end+length-1), str(end+offset) + ":" + str(end))


def reverse(s):
    str = ""
    for i in s:
        str = i + str
    return str


if __name__ == '__main__':

    # The file provided "abc_data.txt" is not adequate to cover off-diagonal and assymetric corner cases
    # I made another file "abc_ext_data.txt" adding 3 more words in a 5x6 grid to examine such cases

    # Choose and read provided or extended data file
    # f = open("abc_data.txt", "r")
    f = open("abc_ext_data.txt", "r")
    lines = f.readlines()
    rows = len(lines) # Number of lines

    # Capture grid size. We don't assume a square grid in our solution
    Nr, Nc = lines[0].split("x")
    Nr, Nc = int(Nr), int(Nc)

    # Capture alphabet grid matrix
    grid = [[0 for _ in range(Nc)] for _ in range(Nr)]
    for row in range(1, Nr+1):
        # We assume no \n or escape chars exist in the middle of each line
        line = lines[row].strip() # Clean leading and trailing spaces or nl
        # print("Line: ", line)
        grid[row-1][:] = line.split(" ")
    #for i in range(Nr):
    #    print(grid[i][:])

    # Capture list of search words
    words = []
    for row in range(Nr+1, rows):
        word = lines[row].strip() # Clean leading and trailing spaces or nl
        words.append(word)
    # print(words, "\n")

    # Now we have a "grid" NrxNc and a list of "words".
    # Let's call the "word_search" function to do the whole search and printing job
    for word in words:
        word_search(grid, Nr, Nc, word)

    f.close()
